class User
  attr_accessor :name, :email, :age, :dog

  def initialize(attributes = {})
    @name  = attributes[:name]
    @email = attributes[:email]
    @age = attributes[:age]
    @dog = attributes[:dog]
  end

  def formatted_email
    "#{@name} <#{@email}> Age:#{@age} Dogname: #{@dog}"
  end
end